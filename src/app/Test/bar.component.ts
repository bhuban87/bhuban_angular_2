import { Component } from '@angular/core';

import { CountService }      from './count.service';

@Component({
  selector: 'app-bar',
  template: '<div><legend>Bar Component</legend><button (click)="decCount()">Decrement{{data}}</button></div>'
})
export class BarComponent {

  data="data";
  constructor(private countService: CountService) { }

  decCount() {
    let x= this.countService.decrement();
    this.data="hello"+x;
  }

}