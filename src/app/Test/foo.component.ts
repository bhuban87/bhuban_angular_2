import { Component } from '@angular/core';

import { CountService }      from './count.service';

@Component({
  selector: 'app-foo',
  template: '<div><legend>Foo Component</legend><button (click)="incCount()">Increment{{data}}</button></div>'
})
export class FooComponent {

    data="data";
  constructor(private countService: CountService) { }
  
  incCount() {
   let x= this.countService.increment();
    this.data="hello"+x;

  }

}