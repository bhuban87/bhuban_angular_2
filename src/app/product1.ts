import { Component } from '@angular/core';
import { AppComponent } from './app.component';
@Component ({
   selector: 'app-root',
   templateUrl: './product1.html',
   providers:[AppComponent]
})
export   class   Product1  {
    hello=null;
    numItemSelected=1;
    intialItemValue=15;
    updatedValue=null;
    itemValue=null;
    constructor(private parentData: AppComponent) {
        this.hello=this.parentData.second;
        this.updatedValue=this.intialItemValue*this.numItemSelected;
    }
    valueChange(event)
    {
        let x=10;
        this.numItemSelected=event.target.value;
        this.updatedValue=this.intialItemValue*this.numItemSelected;
    }
}