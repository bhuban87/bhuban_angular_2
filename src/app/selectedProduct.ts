import { Component ,NgZone ,EventEmitter, Output ,HostListener} from '@angular/core';
import { Injectable } from '@angular/core';
import { RouterModule, Routes,Router,NavigationExtras ,Params,ActivatedRoute } from '@angular/router';
import {CartDataService} from './cart/cartDataService';
import {StoreService} from './services/store.service';
import { ItemsBean } from './beans/MyItem';
import {NoSanitizePipe} from './filter/NoSanitizePipe'

@Component ({
   selector: 'select-product',
   templateUrl: './selectedProduct.html'  
})

export   class   SelectedProduct  {
    selectedProduct:any;
    element: any;
    selectedImagePick:String;
    cartCount:number;
    selectedItem:ItemsBean=null;
    testhtml=null;
    @Output() valueChange = new EventEmitter();
    constructor
    (private router: Router,private zone: NgZone,
      private cartSevice:CartDataService,private paramRoute: ActivatedRoute,
      private store:StoreService
    )
     { 
        let selectedInfo={
            "pics":["assets/images/a1.jpg","assets/images/amazon_2.jpg","assets/images/amazon_1.jpg"]
            
        };
        this.selectedProduct=selectedInfo;
     }
     ngOnInit() 
     {
     // this.paramRoute.snapshot.p
       
      //let itemId = this.paramRoute.snapshot.queryParams["itemId"];
      let itemId = this.paramRoute.snapshot.params["itemId"];
      this.selectedItem=this.store.getSelectedItem(itemId);
      console.log("itemid passed ::"+itemId);
     }

     
     mouseDown(event) {
   
          this.element = event.target;
          this.selectedImagePick= this.element.src;
         /*  this.zone.runOutsideAngular(() => {
            window.document.addEventListener('mousemove', this.mouseMove.bind(this));
          }); */
        }
      
        mouseMove(event) {
          event.preventDefault(); 
         var largeimg=this.selectedImagePick;
          this.testhtml = "dddddd";
        
          this.element = event.target;
         // this.element.setAttribute('height', event.clientX + 'px');
         //this.element.setAttribute('width', event.clientX  + 'px');
        }
       
        onMouseOut(): void {
          //this.testhtml = "<p>Hello worleeeeeeeeeeeeeeeeeeed</p>";
        }
        addCart()
        {
          this.cartSevice.increment();
          this.cartSevice.addItems(this.selectedItem,null);
          this.cartCount=this.cartSevice.count;
         // this.cartCount=this.cartSevice.count;
          //this.valueChange.emit(this.cartCount);
        }

}