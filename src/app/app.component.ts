import { Component } from '@angular/core';
import { IProduct } from './product';
import { ProductService } from './productservice';
import { ButtonService } from './button.service';
import {StoreService} from './services/store.service';
import { ItemsBean } from './beans/MyItem';
import { VERSION } from '@angular/core';

import { CountService }   from './Test/count.service';
import {FooComponent} from './Test/foo.component';
import {BarComponent} from './Test/bar.component';
import {CartDataService} from './cart/cartDataService';
import { Ng2DeviceDetectorModule,Ng2DeviceService } from 'ng2-device-detector';

@Component({
  selector: 'app-root',
  templateUrl: './navbar.html',
  styleUrls: ['./app.component.css'],
  providers: [ProductService,ButtonService,StoreService,CountService,CartDataService]
})
export class AppComponent {
  title = 'hello world bhuban';
  first="first";
  second="second";
  third="third";
  loginSpinner=null;
  appList: any[] = [{
    "ID": "1",
    "Name": "One"
  },

  {
    "ID": "2",
    "Name": "Two"
  }
  ];

  iproducts: IProduct[];
  itemList:any;
  deviceInfo = null;
   constructor(private _product: ProductService,private buttonService:ButtonService,
    private store:StoreService  ,
    private deviceService:Ng2DeviceService) 
    {
      this.epicFunction();
    }
   
   ngOnInit() : void {
      this._product.getproducts()
      .subscribe(iproducts => this.iproducts = iproducts);
      this.itemList=this.store.getStoreItem();
   }
   //device type knowing
   epicFunction() {
    console.log('hello `Home` component');
    this.deviceInfo = this.deviceService.getDeviceInfo();
    console.log(this.deviceInfo);
  }
   clicked(data)
   {
     if(data==1)
     {
      console.log("one clicked");
      this.first=this.buttonService.getSecond();
     }
     else if(data==2)
     {
      console.log("2 clicked");
      this.second=this.buttonService.getThird();
     }
     else{
      console.log("3 clicked");
      this.third=this.buttonService.getFirst();
     }
   }
   signup(data)
   {
    this.loginSpinner=true;
      setTimeout (() => {
        console.log("Hello from setTimeout 2o sec");
        this.loginSpinner=false;
      }, 20000);
      console.log("Hello from setTimeout");
      
   }
}
