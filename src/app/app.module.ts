import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';

import { Product1 } from './product1';

import { Product2 } from './product2';

import { Product3 } from './product3';

import { myInputDir } from './DirectivesTest/input.field.dir';

import {Main} from './zoom/main';
import {MoveBackgroundDirective} from './zoom/MoveBackgroundDirective';
import {ImageZoom} from './zoom/image-zoom';

import { RouterModule, Routes } from '@angular/router';
import {SelectedProduct} from './SelectedProduct';
import {ImageZoomModule} from 'angular2-image-zoom';

import {CartType} from './cart/cartType';
import {CartDataService} from './cart/cartDataService';
import {Cart} from './cart/cart';
import {CartShown} from './cart/cartShown';

import {FooComponent} from './Test/foo.component';
import {BarComponent} from './Test/bar.component';
import { Ng2DeviceDetectorModule } from 'ng2-device-detector';
import {NoSanitizePipe} from './filter/NoSanitizePipe'

const appRoutes: Routes = [
  { path: '', redirectTo: '/Product2', pathMatch: 'full'},
  { path: 'Product1', component: Product1 },
  { path: 'Product2', component: Product2 },
  { path: 'Product3', component: Product3 },
  {path:'SelectedProduct/:itemId',component:SelectedProduct},
  {path:'cart',component:Cart}
];
@NgModule({
  declarations: [
    AppComponent,
    myInputDir,
    Product1,
    Product2,
    Product3,
    SelectedProduct,
    Main,
    MoveBackgroundDirective,
    ImageZoom,Cart,
    FooComponent,BarComponent,CartShown,NoSanitizePipe
   
  ],
  imports: [
    BrowserModule ,HttpModule,
    RouterModule.forRoot(appRoutes),
     ImageZoomModule,
     Ng2DeviceDetectorModule.forRoot()
  ],
  providers: [],
  exports:[myInputDir,Product1,Product2,Product3],
  bootstrap: [AppComponent]
})
export class AppModule { 
  
}
