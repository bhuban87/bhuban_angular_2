export class CartType {
    itemId:Number;
    itemName:string;
    itemPrice:Number;
    itemImg:string;
    constructor(itemId: Number, itemName: string,itemPrice:Number,itemImg:string) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.itemPrice = itemPrice;
        this.itemImg=itemImg;
      }
 }