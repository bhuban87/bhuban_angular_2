import { Component, EventEmitter, Input, Output ,Inject } from '@angular/core';
import { Product3 } from './product3';
import {ActivatedRoute} from "@angular/router";
import {StoreService} from './services/store.service';
import {CartDataService} from './cart/cartDataService';
import { RouterModule, Routes,Router,NavigationExtras ,Params } from '@angular/router';
@Component ({
   selector: 'app-root1',
   templateUrl: './product2.html',
   providers:[Product3,StoreService]
})
export   class   Product2 {
    //@Input() 
    ctMsg : string; 
    public firstname: string;
    public lastname: string;
    itemList:any;
   selectProdString:string='/SelectedProduct/';
    constructor(private product3: Product3,private route: ActivatedRoute,
        private store:StoreService, private cartServ:CartDataService,private router: Router) {
        this.ctMsg=this.product3.getMessage();
    }
     ngOnInit() {
        this.route.queryParams.subscribe(params => {
            this.firstname = params["firstname"];
            this.lastname = params["lastname"];
            this.ctMsg=this.firstname !=null?(this.firstname  + " "+this.lastname):this.ctMsg;
            this.ctMsg=this.product3.getMessage();
            //this.ctMsg=this.firstname  + " "+this.lastname;
            this.itemList=this.store.getStoreItemByType(12);
           // this.cartServ.setCredentials("bhuban_muduli_97");
        });

         let param1 = this.route.snapshot.queryParams["itemId"];
      }
      func()
      {
        //this.cartServ.increment();
      }
      selectedItemProc(event,item)
      {
        this.router.navigateByUrl('/SelectedProduct/'+item.itemId); 
      }
}