//import { Injectable } from '@angular/core';

export class ItemsBean
{
    itemId:number;
    itemName:String;
    itemPrice:number;
    imageLocation:String;
    itemQuantity:number;
    constructor(itemId: number, itemName: string,itemPrice:number,imageLocation:String,itemQuantity:number) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.itemPrice = itemPrice;
        this.imageLocation=imageLocation;
        this.itemQuantity=itemQuantity;
      }
}